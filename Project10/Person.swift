//
//  Person.swift
//  Project10
//
//  Created by Muri Gumbodete on 31/03/2022.
//

import UIKit

class Person: NSObject, Codable {
    var name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
